#!/bin/bash
# bash -c "$(curl -L -s https://gitlab.com/dudw/shell/-/raw/main/install_duf.sh)"

case $(uname -m) in
    "x86_64"|"amd64")
        Arch="x86_64"
    ;;
    "arm64"|"armv8"|"aarch64")
        Arch="arm64"
    ;;
    *)
        echo "Unsupported cpu arch: $(uname -m)"
        exit 2
    ;;
esac

case $(uname -s) in
    "Linux")
        Sys="linux"
    ;;
    *)
        echo "Unsupported system: $(uname -s)"
        exit 2
    ;;
esac


DOWNLOAD_URL=$(curl -fsSL https://api.github.com/repos/muesli/duf/releases/latest | grep 'browser_download_url' | cut -d'"' -f4 | grep "${Sys}_${Arch}.tar.gz")

mkdir -p /tmp/duf
cd /tmp/duf

echo -e "Download URL: $1${DOWNLOAD_URL}"
curl  -L -s -o duf.tar.gz $1${DOWNLOAD_URL}

tar xzvf duf.tar.gz
\cp -f duf /usr/bin/
rm -rf /tmp/duf
duf
