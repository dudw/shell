#!/bin/bash
# bash -c "$(curl -L -s https://gitlab.com/dudw/shell/-/raw/main/install_btm.sh)"
# https://github.com/ClementTsang/bottom/releases

 if [[ "$(uname)" == 'Linux' ]]; then
    case "$(uname -m)" in
      'i386' | 'i686')
        MACHINE='i686'
        ;;
      'amd64' | 'x86_64')
        MACHINE='x86_64'
        ;;
      'armv8' | 'aarch64')
       MACHINE='aarch64'
        ;;
      *)
        echo "error: The architecture is not supported."
        exit 1
        ;;
    esac
fi

echo $MACHINE

DOWNLOAD_URL=$(curl -fsSL https://api.github.com/repos/ClementTsang/bottom/releases/latest | grep 'browser_download_url' | cut -d'"' -f4 | grep "${MACHINE}-unknown-linux-musl.tar.gz")

echo -e "Download URL: $1${DOWNLOAD_URL}"

wget $1${DOWNLOAD_URL}

mkdir -p bottom
tar xzvf bottom*.tar.gz -C bottom &&  \cp -f  bottom/btm  /usr/local/bin/ &&  chmod +x /usr/local/bin/btm && rm -rf bottom*
