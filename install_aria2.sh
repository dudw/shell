#!/bin/bash
# bash -c "$(curl -L -s https://gitlab.com/dudw/shell/-/raw/main/install_aria2.sh)"

 if [[ "$(uname)" == 'Linux' ]]; then
    case "$(uname -m)" in
      'i386' | 'i686')
        MACHINE='386'
        ;;
      'amd64' | 'x86_64')
        MACHINE='amd64'
        ;;
      'armv8' | 'aarch64')
        MACHINE='arm64'
        ;;
      *)
        echo "error: The architecture is not supported."
        exit 1
        ;;
    esac
fi

echo $MACHINE

DOWNLOAD_URL=$(curl -fsSL https://api.github.com/repos/P3TERX/Aria2-Pro-Core/releases/latest | grep 'browser_download_url' | cut -d'"' -f4 | grep "${MACHINE}")
echo -e "Download URL: $1${DOWNLOAD_URL}"
curl -LS "$1${DOWNLOAD_URL}" | tar xzC /usr/local/bin
chmod +x /usr/local/bin/aria2c
ls -l /usr/local/bin/aria2c

cat>/etc/systemd/system/aria2.service<<EOF
[Unit]
Description=aria2 server
After=network.target
Wants=network.target

[Service]
Type=simple
PIDFile=/run/aria2.pid
User=root
ExecStart=/usr/local/bin/aria2c --conf-path=/usr/local/etc/aria2.conf
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

configfile="/usr/local/etc/aria2.conf"

if [ ! -f "$configfile" ]; then

echo "add $configfile"

cat>$configfile<<EOF
dir=/data/downloads
disk-cache=64M
file-allocation=none
continue=true
always-resume=false
max-resume-failure-tries=0
remote-time=true
input-file=/data/config/aria2.session
save-session=/data/config/aria2.session
save-session-interval=1
auto-save-interval=20
force-save=false
max-file-not-found=10
max-tries=0
retry-wait=10
connect-timeout=10
timeout=10
max-concurrent-downloads=10
max-connection-per-server=32
split=64
min-split-size=4M
piece-length=1M
allow-piece-length-change=true
lowest-speed-limit=0
max-overall-download-limit=0
max-download-limit=0
disable-ipv6=false
http-accept-gzip=true
reuse-uri=false
no-netrc=true
allow-overwrite=false
auto-file-renaming=true
content-disposition-default-utf8=true
listen-port=36833
dht-listen-port=36833
enable-dht=true
enable-dht6=true
dht-file-path=/data/config/dht.dat
dht-file-path6=/data/config/dht6.dat
dht-entry-point=dht.transmissionbt.com:6881
dht-entry-point6=dht.transmissionbt.com:6881
bt-enable-lpd=true
enable-peer-exchange=true
bt-max-peers=128
bt-request-peer-speed-limit=10M
max-overall-upload-limit=1M
max-upload-limit=0
seed-ratio=1.0
seed-time=0
bt-hash-check-seed=true
bt-seed-unverified=false
bt-tracker-connect-timeout=10
bt-tracker-timeout=10
bt-prioritize-piece=head=128M,tail=128M
rpc-save-upload-metadata=true
follow-torrent=true
pause-metadata=false
bt-save-metadata=true
bt-load-saved-metadata=true
bt-remove-unselected-file=true
bt-force-encryption=true
bt-detach-seed-only=true
user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36 Edg/93.0.961.47
peer-agent=Deluge 1.3.15
peer-id-prefix=-DE13F0-
on-download-stop=/config/script/delete.sh
on-download-complete=/config/script/move.sh
enable-rpc=true
rpc-allow-origin-all=true
rpc-listen-all=true
rpc-listen-port=6800
rpc-secret=password
rpc-max-request-size=10M
rpc-secure=false
console-log-level=notice
quiet=false
summary-interval=0
retry-on-400=true
retry-on-403=true
retry-on-406=true
retry-on-unknown=true
bt-tracker=
EOF

fi

systemctl enable aria2
systemctl restart aria2
systemctl status aria2
