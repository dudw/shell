#!/bin/bash
# bash -c "$(curl -L -s https://gitlab.com/dudw/shell/-/raw/main/install_filebrowser.sh)"
# https://github.com/filebrowser/filebrowser/releases

 if [[ "$(uname)" == 'Linux' ]]; then
    case "$(uname -m)" in
      'i386' | 'i686')
        MACHINE='386'
        ;;
      'amd64' | 'x86_64')
        MACHINE='amd64'
        ;;
      'armv8' | 'aarch64')
        MACHINE='arm64'
        ;;
      *)
        echo "error: The architecture is not supported."
        exit 1
        ;;
    esac
fi

echo $MACHINE

mkdir -p /usr/local/filebrowser
cd /usr/local/filebrowser

DOWNLOAD_URL=$(curl -fsSL https://api.github.com/repos/filebrowser/filebrowser/releases/latest | grep 'browser_download_url' | cut -d'"' -f4 | grep "linux-${MACHINE}")

ps -ef|grep filebrowser|grep -v grep | awk '{print $2}' |xargs kill -9
rm -rf /usr/local/filebrowser/filebrowser

echo -e "Download URL: $1${DOWNLOAD_URL}"
wget $1${DOWNLOAD_URL}

systemctl stop filebrowser 

tar xzvf linux-${MACHINE}-filebrowser.tar.gz
rm -rf linux-${MACHINE}-filebrowser.tar.gz

configfile="/usr/local/filebrowser/config.json"

if [ ! -f "$configfile" ]; then
cat>$configfile<<EOF
{
  "port": 12080,
  "baseURL": "",
  "address": "0.0.0.0",
  "log": "stdout",
  "database": "/usr/local/filebrowser/database.db",
  "root": "/var/www"
}
EOF
fi

cat>/etc/systemd/system/filebrowser.service<<EOF
[Unit]
Description=filebrowser Service
After=network.target
Wants=network.target

[Service]
Type=simple
PIDFile=/run/filebrowser.pid
ExecStart=/usr/local/filebrowser/filebrowser -c /usr/local/filebrowser/config.json
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

systemctl enable filebrowser  
systemctl restart filebrowser 
systemctl status filebrowser
