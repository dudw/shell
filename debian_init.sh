#!/bin/bash
# curl -L -s https://gitlab.com/dudw/shell/-/raw/main/debian_init.sh | bash

apt update 
apt -y install dialog
apt -y install language-pack-zh-hans gnupg2 sudo htop wget curl tree zip unzip vim nano  lrzsz iotop iftop htop ncdu net-tools rsync psmisc sysstat apt-utils socat cron tmux

timedatectl set-local-rtc 1
timedatectl set-timezone Asia/Shanghai
localectl  set-locale LANG=zh_CN.utf8

#curl -o /usr/local/bin/z.lua https://hub.gitmirror.com/https://github.com/skywind3000/z.lua/blob/master/z.lua
#echo 'eval "$(lua /usr/local/bin/z.lua --init bash)"' >>~/.bashrc