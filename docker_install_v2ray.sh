#!/bin/bash
# curl -L -s https://gitlab.com/dudw/shell/-/raw/main/docker_install_v2ray.sh | bash

mkdir -p /usr/local/etc/v2ray
cd  /usr/local/etc/v2ray

cat>/usr/local/etc/v2ray/config.json<<EOF
{
    "inbounds": [
        {
            "port": 53080,
            "protocol": "vmess",
            "settings": {
                "clients": [
                    {
                        "id": "b0fbf06d-1123-4691-8526-9ae07653f2d1"
                    }
                ]
            }
        },
        {
            "port": 53081,
            "protocol": "vmess",
            "settings": {
                "clients": [
                    {
                        "id": "b0fbf06d-1123-4691-8526-9ae07653f2d1"
                    }
                ]
            },
            "streamSettings": {
                "network": "kcp",
                "kcpSettings": {
                    "header": {
                        "type": "utp"
                    },
                    "seed": "b0fbf06d-1123-4691-8526-9ae07653f2d1"
                }
            }
        },
        {
            "port": 53082,
            "protocol": "vmess",
            "settings": {
                "clients": [
                    {
                        "id": "b0fbf06d-1123-4691-8526-9ae07653f2d1"
                    }
                ]
            },
            "streamSettings": {
                "network": "ws",
                "security": "none"
            }
        }
    ],
    "outbounds": [
        {
            "protocol": "freedom"
        }
    ]
}
EOF

cat>/usr/local/etc/v2ray/docker-compose.yml<<EOF
version: "3.8"

services:
  v2ray:
    container_name: v2ray
    image: v2fly/v2fly-core:v4.45.2
    volumes:
      - /usr/local/etc/v2ray:/etc/v2ray
    network_mode: host
    restart: unless-stopped
EOF
docker-compose stop
docker-compose up -d