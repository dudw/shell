#!/bin/bash
# bash -c "$(curl -L -s https://gitlab.com/dudw/shell/-/raw/main/install_xray.sh)"

bash -c "$(curl -L https://github.com/XTLS/Xray-install/raw/main/install-release.sh)" @ install

## config

if [ ! -f "/usr/local/etc/xray/config.json" ]; then
cat>/usr/local/etc/xray/config.json<<EOF
{
    "inbounds": [
        {
            "port": 55080,
            "protocol": "vmess",
            "settings": {
                "clients": [
                    {
                        "id": "b0fbf06d-1123-4691-8526-9ae07653f2d1"
                    }
                ]
            }
        },
        {
            "port": 55081,
            "protocol": "vmess",
            "settings": {
                "clients": [
                    {
                        "id": "b0fbf06d-1123-4691-8526-9ae07653f2d1"
                    }
                ]
            },
            "streamSettings": {
                "network": "kcp",
                "kcpSettings": {
                    "header": {
                        "type": "utp"
                    },
                    "seed": "b0fbf06d-1123-4691-8526-9ae07653f2d1"
                }
            }
        }
    ],
    "outbounds": [
        {
            "protocol": "freedom"
        }
    ]
}
EOF
fi

### service
systemctl daemon-reload
systemctl enable xray  
systemctl restart xray 
systemctl status xray