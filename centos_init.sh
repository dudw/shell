#!/bin/bash
# curl -L -s https://gitlab.com/dudw/shell/-/raw/main/centos_init.sh | bash

setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config

timedatectl set-local-rtc 1
timedatectl set-timezone Asia/Shanghai
localectl  set-locale LANG=zh_CN.utf8
 
systemctl stop firewalld
systemctl disable  firewalld
dnf install epel-release

dnf makecache fast

dnf install -y wget tree zip unzip vim nano lrzsz iotop iftop htop ncdu net-tools rsync deltarpm psmisc sysstat
#dnf install -y langpacks-zh_CN
