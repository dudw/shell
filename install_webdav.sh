#!/bin/bash
# bash -c "$(curl -L -s https://gitlab.com/dudw/shell/-/raw/main/install_webdav.sh)"
# https://github.com/hacdias/webdav/releases

 if [[ "$(uname)" == 'Linux' ]]; then
    case "$(uname -m)" in
      'i386' | 'i686')
        MACHINE='386'
        ;;
      'amd64' | 'x86_64')
        MACHINE='amd64'
        ;;
      'armv8' | 'aarch64')
        MACHINE='arm64'
        ;;
      *)
        echo "error: The architecture is not supported."
        exit 1
        ;;
    esac
fi

echo $MACHINE

mkdir -p /usr/local/webdav
cd /usr/local/webdav

DOWNLOAD_URL=$(curl -fsSL https://api.github.com/repos/hacdias/webdav/releases/latest | grep 'browser_download_url' | cut -d'"' -f4 | grep "linux-${MACHINE}")

echo -e "Download URL: $1${DOWNLOAD_URL}"
wget $1${DOWNLOAD_URL}

tar xzvf linux-${MACHINE}-webdav.tar.gz
rm -rf linux-${MACHINE}-webdav.tar.gz

cat>/etc/systemd/system/webdav.service<<EOF
[Unit]
Description=WebDAV server
After=network.target
Wants=network.target

[Service]
Type=simple
PIDFile=/run/webdav.pid
User=root
ExecStart=/usr/local/webdav/webdav --config /usr/local/webdav/config.yml
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

configfile="/usr/local/webdav/config.yml"

if [ ! -f "$configfile" ]; then

echo "add $configfile"

cat>$configfile<<EOF
address: 0.0.0.0
port: 5006

tls: false
prefix: /

debug: false
noSniff: false
directory: .
permissions: R
rules: []

log:
  # Logging format ('console', 'json'). Default is 'console'.
  format: console
  # Enable or disable colors. Default is 'true'. Only applied if format is 'console'.
  colors: true
  # Logging outputs. You can have more than one output. Default is only 'stderr'.
  outputs:
  - stderr

cors:
  enabled: true
  credentials: true

users:
  - username: dw
    password: 111111
    directory: /home/data/
EOF

fi



systemctl enable webdav
systemctl restart webdav
systemctl status webdav