#!/bin/bash
# bash -c "$(curl -L https://gitlab.com/dudw/shell/-/raw/main/install_trzszsshd.sh)"
# https://github.com/trzsz/tsshd/releases
# https://github.com/trzsz/trzsz-go/releases

apt update 
apt -y install software-properties-common
add-apt-repository ppa:trzsz/ppa
apt update

apt -y install trzsz tsshd
