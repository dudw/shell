#!/bin/bash
# bash -c "$(curl -L -s https://gitlab.com/dudw/shell/-/raw/main/install_docker-compose.sh)"

 if [[ "$(uname)" == 'Linux' ]]; then
    case "$(uname -m)" in
      'i386' | 'i686')
        MACHINE='386'
        ;;
      'amd64' | 'x86_64')
        MACHINE='x86_64'
        ;;
      'armv8' | 'aarch64')
        MACHINE='aarch64'
        ;;
      *)
        echo "error: The architecture is not supported."
        exit 1
        ;;
    esac
fi

echo $MACHINE

DOWNLOAD_URL=$(curl -fsSL https://api.github.com/repos/docker/compose/releases/latest | grep 'browser_download_url' | cut -d'"' -f4 | grep "linux-${MACHINE}" | grep -v sha256)
rm -rf /usr/bin/docker-compose

echo -e "Download URL: $1${DOWNLOAD_URL}"
curl -LS "$1${DOWNLOAD_URL}" -o /usr/bin/docker-compose

chmod +x /usr/bin/docker-compose
ls -l /usr/bin/docker-compose