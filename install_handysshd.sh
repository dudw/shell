#!/bin/bash
# bash -c "$(curl -L https://gitlab.com/dudw/shell/-/raw/main/install_handysshd.sh)" @ https://gh-proxy.ygxz.in/
# https://github.com/nwtgck/handy-sshd/releases

 if [[ "$(uname)" == 'Linux' ]]; then
    case "$(uname -m)" in
      'i386' | 'i686')
        MACHINE='386'
        ;;
      'amd64' | 'x86_64')
        MACHINE='amd64'
        ;;
      'armv8' | 'aarch64')
        MACHINE='arm64'
        ;;
      *)
        echo "error: The architecture is not supported."
        exit 1
        ;;
    esac
fi

echo $MACHINE

mkdir -p /usr/local/bin
cd /usr/local/bin

DOWNLOAD_URL=$(curl -fsSL https://api.github.com/repos/nwtgck/handy-sshd/releases/latest | grep 'browser_download_url' | cut -d'"' -f4 | grep "linux-${MACHINE}.tar.gz")

echo -e "Download URL: $1${DOWNLOAD_URL}"
wget $1${DOWNLOAD_URL}

systemctl stop handy-sshd

tar xzvf handy-sshd-*.tar.gz
rm -rf handy-sshd-*.tar.gz
rm -rf CHANGELOG.md
rm -rf LICENSE
rm -rf README.md

if [ ! -f "/etc/systemd/system/handy-sshd.service" ]; then

cat>/etc/systemd/system/handy-sshd.service<<EOF
[Unit]
Description=handy-sshd-server
After=network.target
Wants=network.target

[Service]
Type=simple
PIDFile=/run/handy-sshd.pid
User=root
ExecStart=/usr/local/bin/handy-sshd -p 32123 -u handysshd:HmMEEF682b4wlbAd
Restart=on-failure

[Install]
WantedBy=multi-user.target

EOF

fi

systemctl enable handy-sshd
systemctl restart handy-sshd
systemctl status handy-sshd