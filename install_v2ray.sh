#!/bin/bash
# bash -c "$(curl -L -s https://gitlab.com/dudw/shell/-/raw/main/install_v2ray.sh)"

bash <(curl -L https://raw.githubusercontent.com/v2fly/fhs-install-v2ray/master/install-release.sh)

## config

if [ ! -f "/usr/local/etc/v2ray/config.json" ]; then
cat>/usr/local/etc/v2ray/config.json<<EOF
{
    "inbounds": [
        {
            "port": 53080,
            "protocol": "vmess",
            "settings": {
                "clients": [
                    {
                        "id": "b0fbf06d-1123-4691-8526-9ae07653f2d1"
                    }
                ]
            }
        },
        {
            "port": 53081,
            "protocol": "vmess",
            "settings": {
                "clients": [
                    {
                        "id": "b0fbf06d-1123-4691-8526-9ae07653f2d1"
                    }
                ]
            },
            "streamSettings": {
                "network": "kcp",
                "kcpSettings": {
                    "header": {
                        "type": "utp"
                    },
                    "seed": "b0fbf06d-1123-4691-8526-9ae07653f2d1"
                }
            }
        }
    ],
    "outbounds": [
        {
            "protocol": "freedom"
        }
    ]
}
EOF
fi

### service
systemctl daemon-reload
systemctl enable v2ray  
systemctl restart v2ray 
systemctl status v2ray