#!/bin/bash
# curl -L -s https://gitlab.com/dudw/shell/-/raw/main/install_brook_1.sh | bash

case $(uname -m) in
    "x86_64"|"amd64")
        Arch="amd64"
    ;;
    "arm64"|"armv8"|"aarch64")
        Arch="arm64"
    ;;
    *)
        echo "Unsupported cpu arch: $(uname -m)"
        exit 2
    ;;
esac

case $(uname -s) in
    "Linux")
        Sys="linux"
    ;;
    *)
        echo "Unsupported system: $(uname -s)"
        exit 2
    ;;
esac

DOWNLOAD_URL="https://github.com/txthinking/brook/releases/latest/download/brook_${Sys}_${Arch}"
echo -e "Download URL: ${DOWNLOAD_URL}"

systemctl stop brook

cd /usr/local/bin
rm -rf brook
curl  -L -s -o brook ${DOWNLOAD_URL}  && chmod +x brook

if [ ! -f "/etc/systemd/system/brook.service" ]; then
cat>/etc/systemd/system/brook.service<<EOF
[Unit]
Description=brook Service
After=network.target
Wants=network.target
[Service]
Type=simple
PIDFile=/run/brook.pid
ExecStart=/usr/local/bin/brook server -l :52080 -p brook
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

systemctl enable brook

fi


systemctl start brook
systemctl status brook