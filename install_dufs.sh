#!/bin/bash
# bash -c "$(curl -L -s https://gitlab.com/dudw/shell/-/raw/main/install_dufs.sh)"
# https://github.com/sigoden/dufs/releases

 if [[ "$(uname)" == 'Linux' ]]; then
    case "$(uname -m)" in
      'i386' | 'i686')
        MACHINE='386'
        ;;
      'amd64' | 'x86_64')
        MACHINE='x86_64'
        ;;
      'armv8' | 'aarch64')
        MACHINE='aarch64'
        ;;
      *)
        echo "error: The architecture is not supported."
        exit 1
        ;;
    esac
fi

echo $MACHINE

mkdir -p /usr/local/bin
cd /usr/local/bin

DOWNLOAD_URL=$(curl -fsSL https://api.github.com/repos/sigoden/dufs/releases/latest | grep 'browser_download_url' | cut -d'"' -f4 | grep "${MACHINE}-unknown-linux")

echo -e "Download URL: $1${DOWNLOAD_URL}"
wget $1${DOWNLOAD_URL}

systemctl stop dufs

tar xzvf dufs-*.tar.gz
rm -rf dufs-*.tar.gz

if [ ! -f "/etc/systemd/system/dufs.service" ]; then

cat>/etc/systemd/system/dufs.service<<EOF
[Unit]
Description=Dufs WebDAV server
After=network.target
Wants=network.target

[Service]
Type=simple
PIDFile=/run/dufs.pid
User=root
ExecStart=/usr/local/bin/dufs -a /@dw:1 -A -p 5006 /home/data
Restart=on-failure

[Install]
WantedBy=multi-user.target

EOF

fi

systemctl enable dufs
systemctl restart dufs
systemctl status dufs