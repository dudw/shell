#!/bin/bash
# curl -L -s https://gitlab.com/dudw/shell/-/raw/main/install_gost.sh | bash

 if [[ "$(uname)" == 'Linux' ]]; then
    case "$(uname -m)" in
      'i386' | 'i686')
        MACHINE='32'
        ;;
      'amd64' | 'x86_64')
        MACHINE='amd64'
        ;;
      'armv8' | 'aarch64')
        MACHINE='armv8'
        ;;
      *)
        echo "error: The architecture is not supported."
        exit 1
        ;;
    esac
fi

echo $MACHINE

cd /usr/local/bin
rm -rf gost

DOWNLOAD_URL=$(curl -fsSL https://api.github.com/repos/ginuerzh/gost/releases/latest | grep 'browser_download_url' | cut -d'"' -f4 | grep "linux-${MACHINE}" )

echo -e "Download URL: ${DOWNLOAD_URL}"
#wget https://hub.gitmirror.com/${DOWNLOAD_URL}

curl  -L -s -o gost-linux-${MACHINE}.gz   https://hub.gitmirror.com/${DOWNLOAD_URL}
gunzip gost-linux-${MACHINE}.gz
mv gost-linux-${MACHINE} gost
chmod +x gost

configfile="/etc/systemd/system/gost.service"

if [ ! -f "$configfile" ]; then
echo "add $configfile"
cat>$configfile<<EOF
[Unit]
Description=gost Service
After=network.target
Wants=network.target

[Service]
Type=simple
PIDFile=/run/gost.pid
ExecStart=/usr/local/bin/gost  -L=ss://AEAD_CHACHA20_POLY1305:password@:54080
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

fi

systemctl enable gost  
systemctl restart gost 
systemctl status gost
