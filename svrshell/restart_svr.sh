#!/bin/bash

RESOURCE_NAME=$1
JARFILE=$2
XARG=$3
XMX=$4
MAX_TIMEOUT=15

tpid=`ps -ef|grep $RESOURCE_NAME|grep -v grep|grep -v kill|grep -v restart_svr|awk '{print $2}'`

if [ -n "${tpid}" ]; then
    echo $RESOURCE_NAME' Stop Process...'
    kill -15 $tpid
fi

for((i=0;i<$MAX_TIMEOUT;i++))
do
    sleep 1
    tpid=`ps -ef|grep $RESOURCE_NAME|grep -v grep|grep -v kill|grep -v restart_svr|awk '{print $2}'`
    if [ -n "${tpid}" ]; then
        echo $RESOURCE_NAME' Stoping.........'$i
    else
        break
    fi
done

if [ -n "${tpid}" ]; then
    echo $RESOURCE_NAME' Kill Process!'
    kill -9 $tpid
else
    echo $RESOURCE_NAME' Stop Success!'
fi

tpid=`ps -ef|grep $RESOURCE_NAME|grep -v grep|grep -v kill|grep -v restart_svr|awk '{print $2}'`

if [ "${tpid}" ]; then
    echo $RESOURCE_NAME' is running.'
else
    echo $RESOURCE_NAME' is NOT running.'
fi

echo $RESOURCE_NAME' Start ...........  java  -jar '  $JARFILE $XARG
(java  -jar  -Xms256m -Xmx$XMX  $JARFILE  $XARG &)
echo $RESOURCE_NAME' Start Success! '
