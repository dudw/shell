#!/bin/bash
# curl -L -s https://gitlab.com/dudw/shell/-/raw/main/centos_init_arm.sh | bash

setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config

yum install -y epel-release
yum install -y langpacks-zh_CN

timedatectl set-local-rtc 1
timedatectl set-timezone Asia/Shanghai
localectl  set-locale LANG=zh_CN.utf8
 
systemctl stop firewalld
systemctl disable  firewalld

curl -o /usr/local/bin/z.lua https://hub.gitmirror.com/https://raw.githubusercontent.com/skywind3000/z.lua/master/z.lua
echo 'eval "$(lua /usr/local/bin/z.lua --init bash)"' >>~/.bashrc

yum install -y wget tree zip unzip vim nano yum-utils lrzsz iotop iftop htop ncdu  net-tools rsync psmisc lua
