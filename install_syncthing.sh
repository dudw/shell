#!/bin/bash
# bash -c "$(curl -L -s https://gitlab.com/dudw/shell/-/raw/main/install_syncthing.sh)"

 if [[ "$(uname)" == 'Linux' ]]; then
    case "$(uname -m)" in
      'i386' | 'i686')
        MACHINE='32'
        ;;
      'amd64' | 'x86_64')
        MACHINE='amd64'
        ;;
      'armv8' | 'aarch64')
        MACHINE='arm64'
        ;;
      *)
        echo "error: The architecture is not supported."
        exit 1
        ;;
    esac
fi

echo $MACHINE

filename=$(curl -fsSL https://api.github.com/repos/syncthing/syncthing/releases/latest | grep 'name' | cut -d'"' -f4 | grep "linux-${MACHINE}")

DOWNLOAD_URL=$(curl -fsSL https://api.github.com/repos/syncthing/syncthing/releases/latest | grep 'browser_download_url' | cut -d'"' -f4 | grep "linux-${MACHINE}")

echo -e "Download URL: $1${DOWNLOAD_URL}"
wget "$1${DOWNLOAD_URL}"

tar xzvf  syncthing-linux-${MACHINE}-*.tar.gz

systemctl stop syncthing

cp -f   syncthing-linux-${MACHINE}-*/syncthing /usr/bin/
rm -rf  syncthing-linux-${MACHINE}-*

cat>/etc/systemd/system/syncthing.service<<EOF
[Unit]
Description=Syncthing - Open Source Continuous File Synchronization
Documentation=man:syncthing(1)
After=network.target
Wants=network.target
StartLimitIntervalSec=60
StartLimitBurst=4

[Service]
User=root
Type=simple
PIDFile=/run/syncthing.pid
ExecStart=/usr/bin/syncthing serve --no-browser --no-restart --logflags=0
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable syncthing
systemctl restart syncthing
systemctl status syncthing
