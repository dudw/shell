#!/bin/bash
# bash -c "$(curl -L -s https://gitlab.com/dudw/shell/-/raw/main/install_brook.sh)"

 if [[ "$(uname)" == 'Linux' ]]; then
    case "$(uname -m)" in
      'i386' | 'i686')
        MACHINE='386'
        ;;
      'amd64' | 'x86_64')
        MACHINE='amd64'
        ;;
      'armv8' | 'aarch64')
        MACHINE='arm64'
        ;;
      *)
        echo "error: The architecture is not supported."
        exit 1
        ;;
    esac
fi

echo $MACHINE


systemctl stop brook
#==============================================
DOWNLOAD_URL=$(curl -fsSL https://api.github.com/repos/txthinking/brook/releases/latest | grep 'browser_download_url' | cut -d'"' -f4 | grep "linux_${MACHINE}" | grep -v ipk)

cd /usr/local/bin
rm -rf brook

echo -e "Download URL: $1${DOWNLOAD_URL}"
curl  -L -s -o brook $1${DOWNLOAD_URL}  && chmod +x brook

cat>/etc/systemd/system/brook.service<<EOF
[Unit]
Description=brook Service
After=network.target
Wants=network.target
[Service]
Type=simple
PIDFile=/run/brook.pid
ExecStart=/usr/local/bin/brook server -l :52080 -p brook
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

systemctl enable brook  
systemctl restart brook
systemctl status brook


