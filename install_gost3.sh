#!/bin/bash
# bash -c "$(curl -L -s https://gitlab.com/dudw/shell/-/raw/main/install_gost3.sh)"

if [[ "$EUID" -ne '0' ]]; then
    echo "$(tput setaf 1)Error: You must run this script as root!$(tput sgr0)"
    exit 1
fi

# Set the desired GitHub repository
repo="go-gost/gost"
base_url="https://api.github.com/repos/$repo/releases"


# Detect the operating system
if [[ "$(uname)" == "Linux" ]]; then
    os="linux"
elif [[ "$(uname)" == "Darwin" ]]; then
    os="darwin"
elif [[ "$(uname)" == "MINGW"* ]]; then
    os="windows"
else
    echo "Unsupported operating system."
    exit 1
fi

# Detect the CPU architecture
arch=$(uname -m)
case $arch in
    x86_64)
        cpu_arch="amd64"
        ;;
    armv5*)
        cpu_arch="armv5"
        ;;
    armv6*)
        cpu_arch="armv6"
        ;;
    armv7*)
        cpu_arch="armv7"
        ;;
    aarch64)
        cpu_arch="arm64"
        ;;
    i686)
        cpu_arch="386"
        ;;
    mips64*)
        cpu_arch="mips64"
        ;;
    mips*)
        cpu_arch="mips"
        ;;
    mipsel*)
        cpu_arch="mipsle"
        ;;
    *)
        echo "Unsupported CPU architecture."
        exit 1
        ;;
esac


# Retrieve available versions from GitHub API
versions=$(curl -s "$base_url" | grep -oP 'tag_name": "\K[^"]+')
version=$(echo "$versions" | head -n 1)

mkdir -p /usr/local/gost3
cd /usr/local/gost3
    
get_download_url="$base_url/tags/$version"
download_url=$(curl -s "$get_download_url" | grep -Eo "\"browser_download_url\": \".*${os}.*${cpu_arch}.*\"" | awk -F'["]' '{print $4}' | head -n 1)

echo $1$download_url
# Download the binary
echo "Downloading gost version $version"

wget -O gost.tar.gz $1$download_url

systemctl stop gost3

# Extract and install the binary
echo "Installing gost..."
tar -xzf gost.tar.gz
chmod +x gost

rm -rf gost.tar.gz

echo "gost installation completed!"

configfile="/etc/systemd/system/gost3.service"

if [ ! -f "$configfile" ]; then
echo "add $configfile"
cat>$configfile<<EOF
[Unit]
Description=gost Service
After=network.target
Wants=network.target

[Service]
Type=simple
PIDFile=/run/gost.pid
ExecStart=/usr/local/gost3/gost -L ss+kcp://chacha20-ietf-poly1305:password@:56080
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

fi

systemctl enable gost3
systemctl restart gost3
systemctl status gost3
